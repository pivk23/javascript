import { updateChart } from "./chart.js";
import { displayBestCombination } from "./ui.js";

/**
 * Genethic algorithm implementation
 *
 * @class GA
 */
class GA {
  /**
   * Creates an instance of GA.
   * @param {Object} items fenotype of current optimization problem
   * @param {Number} [populationCap=25] number of max population inside one generation
   * @param {Number} [maxCapacity=400] parameter for current optimization problem - effects fitness function
   * @param {Number} [maxGeneration=100] max number of iterations of evolution process
   * @memberof GA
   */
  constructor(
    items,
    populationCap = 25,
    maxCapacity = 400,
    maxGeneration = 100
  ) {
    this.items = items;
    this.populationCap = populationCap;
    this.maxCapacity = maxCapacity;
    this.maxGeneration = maxGeneration;
  }

  init() {
    //initialize population
    this.population = new Population(
      this.items,
      this.populationCap,
      this.maxCapacity
    );

    while (this.maxGeneration--) {
      //make offspring
      let offsprings = [];
      for (let i = 0; i < 50; i++) {
        //select random parents from initial population
        let parents = this.population.select();

        //crossover parents
        let offs_temp = this.population.crossover(parents[0], parents[1]);
        offsprings.push(offs_temp[0]);
        offsprings.push(offs_temp[1]);
      }

      //mutate offspring and update its fitness
      for (let i = 0; i < offsprings.length; i++) {
        offsprings[i].mutate();
        offsprings[i].calculateFitness(this.items, this.maxCapacity);
        offsprings[i].generation = 100 - this.maxGeneration;
      }

      //update initial population with better offspring
      this.population.miAlfa(offsprings);

      //update chart
      let averageFitness = this.population.chromosomes
        .map((item) => item.fitness)
        .reduce((total, item) => item + total);
      averageFitness /= this.population.chromosomes.length;
      updateChart(averageFitness, 100 - this.maxGeneration);
    }
    //update ui
    displayBestCombination(this.population.chromosomes[0], this.items);
  }
}

class Chromosome {
  constructor(items, maxCapacity) {
    (this.generation = 0), (this.genotype = []), (this.fitness = 0);
    this.encode(items, maxCapacity);
    this.calculateFitness(items, maxCapacity);
  }

  // make a random chromosome genotype from available items
  encode(items, maxCapacity) {
    this.genotype = Array(items.length).fill(0);

    let totalWeight = 0;
    while (totalWeight < maxCapacity) {
      let index = Math.floor(Math.random() * items.length);
      totalWeight += items[index].weight;
      if (totalWeight >= maxCapacity) {
        break;
      }
      this.genotype[index] = 1;
    }
  }

  //calculate chromosome's fitness
  calculateFitness(items, maxCapacity) {
    let selectedItems = items.filter((item, key) => {
      return this.genotype[key];
    });

    if (!Array.isArray(selectedItems) || !selectedItems.length) {
      this.fitness = 0;
    } else {
      this.fitness = selectedItems
        .map((item) => item.value)
        .reduce((total, item) => item + total);
      let totalWeight = selectedItems
        .map((item) => item.weight)
        .reduce((total, item) => item + total);

      if (totalWeight > maxCapacity) {
        this.fitness = 0;
      }
    }
  }

  //mutate chromosome
  mutate(probability = 0.1) {
    for (let i = 0; i < this.genotype.length; i++) {
      let random = Math.random();
      if (random <= probability) {
        if (this.genotype[i] === 1) {
          this.genotype[i] = 0;
        } else {
          this.genotype[i] = 1;
        }
      }
    }
  }
}

class Population {
  constructor(items, populationCap, maxCapacity) {
    (this.items = items),
      (this.maxCapacity = maxCapacity),
      (this.chromosomes = []);

    while (populationCap--) {
      let chromosome = new Chromosome(items, maxCapacity);
      this.chromosomes.push(chromosome);
    }
  }

  //crossover two parents and return offspring
  crossover(parent1, parent2, probability = 0.75) {
    if (Math.random() <= probability) {
      let point = Math.floor(Math.random() * parent1.genotype.length);
      point = point == 0 ? 1 : point;

      let head1 = parent1.genotype.slice(0, point);
      let head2 = parent2.genotype.slice(0, point);
      let tail1 = parent1.genotype.slice(point);
      let tail2 = parent2.genotype.slice(point);

      let offspring1 = new Chromosome(this.items, this.maxCapacity);
      let offspring2 = new Chromosome(this.items, this.maxCapacity);
      offspring1.genotype = head1.concat(tail2);
      offspring2.genotype = head2.concat(tail1);
      offspring1.calculateFitness(this.items, this.maxCapacity);
      offspring2.calculateFitness(this.items, this.maxCapacity);

      return [offspring1, offspring2];
    }
    return [parent1, parent2];
  }

  //select(return) two best parents from current population
  select() {
    let selection = [];
    for (let i = 0; i < this.chromosomes.length; i++) {
      if (Math.round(Math.random()) == 1) {
        selection.push(this.chromosomes[i]);
      }
    }
    selection.sort((a, b) => {
      return a.fitness < b.fitness;
    });
    return [selection[0], selection[1]];
  }

  //update current population with the best offspring
  miAlfa(offsprings) {
    let newGeneration = this.chromosomes.concat(offsprings);
    newGeneration.sort((a, b) => {
      return b.fitness - a.fitness;
    });
    this.chromosomes = newGeneration.slice(0, 25);
  }
}

export { GA };
