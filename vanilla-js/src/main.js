// open/close cart dropdown menu
const cart = document.getElementById("cart");
cart.addEventListener("click", handleOpen);

function handleOpen(event) {
  event.preventDefault();

  const dropdown = document.getElementById("cart-dropdown");
  dropdown.style.visibility =
    dropdown.style.visibility == "visible" ? "hidden" : "visible";
}

let cartLength = 0;
// update button inside the dropdown menu
function updateBuyNowBtn() {
  const buyNow = document.getElementById("buy-now");
  if (cartLength == 0) {
    buyNow.disabled = true;
    buyNow.style.background = "#E5E5E5";
    buyNow.style.color = "#9093A6";
    buyNow.style.cursor = "auto";
  } else {
    buyNow.disabled = false;
    buyNow.style.background = "#ffe145";
    buyNow.style.color = "#b79c10";
    buyNow.style.cursor = "pointer";

    buyNow.onmouseover = () => {
      buyNow.style.background = "#fff7cc";
    };
    buyNow.onmouseout = () => {
      buyNow.style.background = "#ffe145";
    };
  }
}
updateBuyNowBtn();

// "Stavi u košaricu" buttons
const orderBtns = document.getElementsByClassName("add_to_basket");
for (let i = 0; i < orderBtns.length; i++) {
  orderBtns[i].addEventListener("click", handleAddToBasket);
  enableAddToBasketBtn(i);
}

function enableAddToBasketBtn(id) {
  const button = document.getElementsByClassName("add_to_basket")[id];
  button.disabled = false;
  button.style.background = "#ffe145";
  button.style.color = "#b79c10";
  button.style.cursor = "pointer";
  button.onmouseover = () => {
    button.style.background = "#fff7cc";
  };
  button.onmouseout = () => {
    button.style.background = "#ffe145";
  };
}

function disableAddToBasketBtn(id) {
  const button = document.getElementsByClassName("add_to_basket")[id];
  button.disabled = true;
  button.style.background = "#E5E5E5";
  button.style.color = "#9093A6";
  button.style.cursor = "auto";
}

function handleAddToBasket(event) {
  event.preventDefault();

  disableAddToBasketBtn(this.id);

  let items = document.querySelectorAll(".buyable");

  let img_src = items[this.id].children[0].getAttribute("src");
  let name = items[this.id].children[1].children[0].innerHTML;
  let price = items[this.id].children[1].children[1].children[0].innerHTML;
  let amount = items[this.id].children[1].children[2].children[1].value;
  let total = price * amount;

  let cartItem = document.createElement("div");
  cartItem.setAttribute("class", "cart-item flex");
  cartItem.setAttribute("aria-label", "Cart Item");
  cartItem.innerHTML = `
    <img class="cart-item-img" src=${img_src}>
    <div aria-label="Cart Item Description" class="item-description flex">
        <div class="item-title">
            ${name}
        </div>
        <div class="item-details flex">
            <p>Cijena: <span style="font-weight:700;">${total},00</span><span style="font-weight:700;"> kn</span></p>
            <p>Količina: <span style="font-weight:700;">${amount}</span></p>
        </div>
    </div>
    <button aria-label="Delete Item" id="${this.id}" class="delete-button"><img src="../assets/icons/button-delete.svg" class="delete-item-img"></button>
`;

  document.querySelector("div#cart-content").appendChild(cartItem);
  cartLength++;
  document.querySelector("div#cart").children[1].innerHTML = cartLength;

  updateBuyNowBtn();

  cartItem.children[2].addEventListener("click", handleDeleteItem);

  const dropdown = document.getElementById("cart-dropdown");
  dropdown.style.visibility = "visible";

  if (cartLength > 3) {
    showSlider();
  }
}

// delete item from the basket
function handleDeleteItem(event) {
  event.preventDefault();

  this.parentElement.parentElement.removeChild(this.parentElement);
  let previousLength = cartLength;
  cartLength--;
  document.querySelector("div#cart").children[1].innerHTML = cartLength;
  updateBuyNowBtn();

  enableAddToBasketBtn(this.id);

  if (previousLength == 4) {
    killSlider();
  }
}

// slider functions
function showSlider() {
  const cartMenu = document.querySelector("div#cart-dropdown");
  cartMenu.children[1].style.display = "none";
  cartMenu.children[3].style.display = "none";

  const upButton = document.createElement("button");
  upButton.innerHTML = "&uarr;";
  upButton.className = "cart-nav";
  upButton.addEventListener("click", handleUp);
  const cartContent = document.querySelector("div#cart-content");
  cartMenu.insertBefore(upButton, cartContent);

  const downButton = document.createElement("button");
  downButton.innerHTML = "&darr;";
  downButton.className = "cart-nav";
  downButton.addEventListener("click", handleDown);
  const buyNowBtn = document.querySelector("button#buy-now");
  cartMenu.insertBefore(downButton, buyNowBtn);

  cartContent.style.height = "261px";
  cartContent.style.overflowY = "auto";

  let items = document.getElementsByClassName("cart-item");
  items[3].style.display = "none";
}

function handleUp() {
  let items = document.getElementsByClassName("cart-item");
  items[0].style.display = "flex";
  items[3].style.display = "none";
}

function handleDown() {
  let items = document.getElementsByClassName("cart-item");
  items[0].style.display = "none";
  items[3].style.display = "flex";
}

function killSlider() {
  const cartMenu = document.querySelector("div#cart-dropdown");
  cartMenu.removeChild(cartMenu.children[2]);
  cartMenu.removeChild(cartMenu.children[4]);

  for (let i = 0; i < cartMenu.children[2].children.length; i++) {
    cartMenu.children[2].children[i].style.display = "flex";
  }
  cartMenu.children[1].style.display = "flex";
  cartMenu.children[3].style.display = "flex";

  const cartContent = document.querySelector("div#cart-content");
  cartContent.style.height = "auto";
}
