<!DOCTYPE html>
<html lang="hr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <script type="module" src="src/main.js"></script>
    <title>Čvarci.net</title>
</head>

<body>
    <nav aria-label="Primary" id="navbar" class="flex">
        <div class="nav_item flex">
            <div role="button" aria-label="Menu" aria-haspopup="true" aria-expanded="false" id="menu_button" class="flex" onclick="">
                <img src="assets\icons\menu_button.svg" alt="menu_button">
            </div>
        </div>
        <div class="nav_item flex">
            <img role="button" aria-label="Logo Home" id="logo" src="assets\logos\cvarci_logo.svg" alt="čvarci.net" onclick="">
        </div>
        <ul role="list" aria-label="My Account Links" id="buttons" class="flex nav_item">
            <li role="button listItem" aria-label="Login" class="flex">
                <img id="person-img" src="assets\icons\person.svg" alt="person">
                <span>PRIJAVI SE</span>
            </li>
            <li role="listItem" aria-label="Cart" class="flex">
                <div role="button" aria-haspopup="menu" id="cart" class="flex">
                    <img id="cart-img" src="assets\icons\cart.svg" alt="cart">
                    <span aria-label="Amount of Items in a Cart">0</span>
                </div>
                <div role="menu" aria-label="Cart Content" aria-expanded="false" class="flex" id="cart-dropdown">
                    <h3 id="cart-title">Moja košarica</h3>
                    <div id="cart-line-container">
                        <img src="assets\icons\line.svg" alt="line">
                    </div>
                    <div aria-label="Cart Content" id="cart-content">
                    </div>
                    <div id="cart-line-container">
                        <img id="cart-line" src="assets\icons\line.svg" alt="line">
                    </div>
                    <button id="buy-now">Kupi sada</button>
                </div>
            </li>
        </ul>
    </nav>

    <section role="banner" id="landing_content" class="flex">
        <div id="bc_container" class="flex">
            <div id="banner_content" class="flex">
                <h1>
                    Najbolji čvarci na kućnom pragu
                </h1>
                <div aria-label="Get Čvarci" id="landing_buttons" class="flex">
                    <div role="button" aria-label="Order" class="flex landing_button" onclick="">
                        <p class="lbtn_text bold">DOSTAVA</p>
                        <p class="lbtn_text">Naruči</p>
                    </div>
                    <div role="button" aria-label="Collect" class="flex landing_button" onclick="">
                        <p class="lbtn_text bold">PREUZMI</p>
                        <p class="lbtn_text">Na čvarkomatu</p>
                    </div>
                </div>
            </div>

        </div>
        <div id="banner_photo" class="flex">
            <img src="assets\images\banner.png" alt="banner photo">
        </div>
    </section>

    <section aria-label="Delivery Information" id="delivery">
        <ul role="list" class="flex">
            <li role="listItem" id="time" class="flex">
                <img class="icon" src="assets\icons\time-eat.svg" alt="timetoeat">
                <p>dostavljamo čvarke za manje od 30 minuta</p>
            </li>
            <li role="listItem" id="driver" class="flex">
                <img class="icon" src="assets\icons\delivery.svg" alt="delivery">
                <p>naši šoferi voze tomose</p>
            </li>
            <li role="listItem" id="france" class="flex">
                <img class="icon" src="assets\icons\paris.svg" alt="delivery">
                <p>nabavljamo najbolje prasce iz Francuske</p>
            </li>
        </ul>
    </section>

    <section aria-label="Buy Online" id="buy_online" class="flex">
        <h2>Novo u ponudi ! naručite čvarke online</h2>
        <ul role="list" id="cards">
            <li role="listItem" class="buyable">
                <img loading="lazy" class="item-img" src="assets\images\cvarci1.png" alt="cvarci1">
                <div>
                    <h3 class="item-name">Čvarci domaći</h3>
                    <p class="item-price"><span>50</span><span> kn / kg</span></p>
                    <form class="item-amount flex">
                        <label for="amount" class="amount-label">Količina: </label>
                        <input type="number" value="1" min="1" max="9" class="amount">
                    </form>
                    <button type="submit" class="add_to_basket" id='0'>Stavi u košaricu</button>
                </div>
            </li>
            <li role="listItem" class="buyable">
                <img loading="lazy" class="item-img" src="assets\images\cvarci2.png" alt="cvarci2">
                <div>
                    <h3 class="item-name">Čvarci slavonski</h3>
                    <p class="item-price"><span>80</span><span> kn / kg</span></p>
                    <form class="item-amount flex">
                        <label for="amount" class="amount-label">Količina: </label>
                        <input type="number" value="1" min="1" max="9" onChange="" class="amount">
                    </form>
                    <button type="submit" class="add_to_basket" id='1'>Stavi u košaricu</button>
                </div>
            </li>
            <li role="listItem" class="buyable">
                <img loading="lazy" class="item-img" src="assets\images\cvarci3.png" alt="cvarci3">
                <div>
                    <h3 class="item-name">Čvarci pileći</h3>
                    <p class="item-price"><span>60</span><span> kn / kg</span></p>
                    <form class="item-amount flex">
                        <label for="amount" class="amount-label">Količina: </label>
                        <input type="number" value="1" min="1" max="9" onChange="" class="amount">
                    </form>
                    <button type="submit" class="add_to_basket" id='2'>Stavi u košaricu</button>
                </div>
            </li>
            <li role="listItem" class="buyable">
                <img loading="lazy" class="item-img" src="assets\images\cvarci4.png" alt="cvarci4">
                <div>
                    <h3 class="item-name">Čvarci od divljači</h3>
                    <p class="item-price"><span>100</span><span> kn / kg</span></p>
                    <form class="item-amount flex">
                        <label for="amount" class="amount-label">Količina: </label>
                        <input type="number" value="1" min="1" max="9" onChange="" class="amount">
                    </form>
                    <button type="submit" class="add_to_basket" id='3'>Stavi u košaricu</button>
                </div>
            </li>
        </ul>
    </section>

    <section id="restaurant" class="flex">
        <div id="r_title" class="flex">
            <h2>Naše čvarke možete pronaći </h2>
            <button aria-haspopup="true" aria-expanded="false" id="show_all" onclick="">
                <p>prikaži sve</p>
            </button>
        </div>
        <div id="cards_container">
            <ul role="list" id="cards">
                <li role="listItem img" aria-label="Restaurant Burger d'Lice" class="single_restaurant flex">
                    <img loading="lazy" class="meal" src="assets\images\meal1.png" alt="meal1">
                    <img loading="lazy" class="r_logo" src="assets\logos\restaurant1.png" alt="restaurant1">
                </li>
                <li role="listItem img" aria-label="Restaurant Il Pastifigio" class="single_restaurant flex">
                    <img loading="lazy" class="meal" src="assets\images\meal2.png" alt="meal2">
                    <img loading="lazy" class="r_logo" src="assets\logos\restaurant2.png" alt="restaurant2">
                </li>
                <li role="listItem img" aria-label="Restaurant Believe" class="single_restaurant flex">
                    <img loading="lazy" class="meal" src="assets\images\meal3.png" alt="meal3">
                    <img loading="lazy" class="r_logo" src="assets\logos\restaurant3.png" alt="restaurant3">
                </li>
                <li role="listItem img" aria-label="Restaurant Vietnamese Urban Food" class="single_restaurant flex">
                    <img loading="lazy" class="meal" src="assets\images\meal4.png" alt="meal4">
                    <img loading="lazy" class="r_logo" src="assets\logos\restaurant4.png" alt="restaurant4">
                </li>
            </ul>
            <div class="flex lr">
                <button aria-label="restaurants left" id="left" class="flex" onclick="">
                    <img loading="lazy" src="assets\icons\left.svg">
                </button>
                <button aria-label="restaurants right" id="right" class="flex" onclick="">
                    <img loading="lazy" src="assets\icons\right.svg">
                </button>
            </div>
        </div>
    </section>

    <section id="branding_partner" class="flex">
        <div id="bt_container" class="flex">
            <div id="branding_title" class="flex">
                <h2>Želite biti naš brand partner ?</h2>
                <p>Pošaljite nam Vaš broj i kontaktirat ćemo Vas u najkraćem mogućem roku</p>
            </div>
        </div>
        <form id="form" class="flex">
            <input type="email" placeholder="Pošaljite Vašu email adresu" id="femail">
            <input role="button" type="submit" id="submit" class="flex" value="pošalji" onclick="">
        </form>
    </section>

    <section id="map" class="flex">
        <h2>Gdje se nalaze naši čvarkomati ?</h2>
        <img loading="lazy" src="assets\images\map.png" alt="map">
    </section>

    <section aria-label="General Information" id="general" class="flex">
        <div id="general_container" class="flex">
            <div id="general_info">
                <div role="img" aria-label="1" class="flex general_info_child">
                    <img loading="lazy" src="assets\images\1.png" alt="1">
                    <p>klaonica</p>
                </div>
                <div role="img" aria-label="6" class="flex general_info_child">
                    <img loading="lazy" src="assets\images\6.png" alt="6">
                    <p>vrsta čvaraka</p>
                </div>
                <div role="img" aria-label="11" class="flex general_info_child">
                    <img loading="lazy" src="assets\images\11.png" alt="11">
                    <p>restorana</p>
                </div>
                <div role="img" aria-label="1" class="flex general_info_child">
                    <img loading="lazy" src="assets\images\1.png" alt="1">
                    <p>najbolja cijena</p>
                </div>
            </div>
        </div>
    </section>

    <section role="img" aria-label="Instagram photos of Cvarci.net" id="instagram" class="flex">
        <h2 id="text_container" class="flex">
            <span id="hashtag">#čvarcinet</span><span id="ig">na instagramu</span>
        </h2>
        <div id="photos_grid">
            <img loading="lazy" src="assets\images\ig01.png" alt="instagram01">
            <img loading="lazy" src="assets\images\ig02.png" alt="instagram02">
            <img loading="lazy" src="assets\images\ig03.png" alt="instagram03">
            <img loading="lazy" src="assets\images\ig04.png" alt="instagram04">
        </div>
    </section>

    <footer aria-label="Secondary Navigation">
        <div id="upper" class="flex">
            <img loading="lazy" role="button img" aria-label="Logo Home" id="logo" src="assets\logos\cvarci_logo.svg" alt="čvarci.net" onclick="">
            <ul role="list" aria-label="Information Links" id="links1" class="flex">
                <li role="button listItem" onclick="">O nama</li>
                <li role="button listItem" onclick="">Cijenik</li>
                <li role="button listItem" onclick="">Kontakt</li>
            </ul>
        </div>
        <div id="line_container" class="flex">
            <img loading="lazy" id="line" src="assets\icons\line.svg" alt="line">
        </div>
        <div id="lower" class="flex">
            <ul role="list" aria-label="Social Media Links" id="links2" class="flex">
                <li><img loading="lazy" role="button" aria-label="Instagram" class="sm_icon" src="assets\icons\instagram.svg" alt="instagram" onclick=""></li>
                <li><img loading="lazy" role="button" aria-label="Twitter" class="sm_icon" src="assets\icons\twitter.svg" alt="twitter" onclick=""></li>
                <li><img loading="lazy" role="button" aria-label="Facebook" class="sm_icon" src="assets\icons\facebook.svg" alt="facebook" onclick=""></li>
            </ul>
            <ul role="list" aria-label="Documents" id="links3" class="flex">
                <li role="button listItem" onclick="">Polica privatnosti</li>
                <li role="button listItem" onclick="">Uvjeti korištenja<span></li>
                <li role="listItem">© 2021 čvarci.net</li>
            </ul>
        </div>
    </footer>
</body>

</html>